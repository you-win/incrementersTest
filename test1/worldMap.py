class worldMap():
	def worldMapLogic(self):
		worldMap = []
		for i in range(0,5):
			worldMap.append(["O"]*5)	
		betterWorldMap = {0:{0:"home",1:"wild",2:"wild",3:"city walls",4:"city center"},
			1:{0:"path",1:"wild",2:"fortress outskirts",3:"city walls",4:"city walls"},
			2:{0:"path",1:"fortress outskirts",2:"fortress",3:"fortress outskirts",4:"wild"},
			3:{0:"wild",1:"wild",2:"fortress outskirts",3:"battlegrounds",4:"battlegrounds"},
			4:{0:"wild",1:"wild",2:"battlegrounds",3:"battlegrounds",4:"battlegrounds"}}
		#setting the player position
		playerPositionX = [0]
		playerPositionY = [0]
		worldMap[playerPositionY[0]][playerPositionX[0]] = "X"
