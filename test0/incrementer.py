__author__ = 'you-win'

#importing functions
import random
import time

#creating the initial table data
playerAttributes = []
playerStatsInfo = ["Player Health","Player Gold Count","Common Loot","Uncommon Loot","Rare Loot"]

for i in range(0,1):
	playerAttributes.append([0]*5)
playerAttributes[0][0] += 10

monstersNo = ["1. Rat","2. Slime","3. Knight","4. Royal guard","5. Sorcerer","6. Doppelganger"]
monsters = ["Rat","Slime","Knight","Royal Guard","Sorcerer","Doppelganger"]
monstersStat = [1,2,4,8,16,32]

#monster dictionary instead of separate lists

monsterExamine = {"Rat":"Scurries about all innocent-like.",
				"Slime":"Gooey.",
				"Knight":"Might be a fair prince, might be your average scrub.",
				"Royal Guard":"Not actually royalty.",
				"Sorcerer":"Kind of a bitch.",
				"Doppelganger":"It's you!"}
				
#monster locations (look more into this)

ratLocation = ("home","wild","city walls","city center","fortress outskirts","fortress","battlegrounds")
slimeLocation = ("wild","city walls","city center","fortress outskirts","fortress","battlegrounds")
knightLocation = ("path","city walls","city center","fortress outskirts","fortress","battlegrounds")
royalGuardLocation = ("city center","fortress","battlegrounds")
sorcererLocation = ("city center","battlegrounds")
doppelgangerLocation = ("home","fortress")

monsterLocations = {"Rat":ratLocation,"Slime":slimeLocation,"Knight":knightLocation,"Royal Guard":royalGuardLocation,"Sorcerer":sorcererLocation,"Doppelganger":doppelgangerLocation}

#sell locations

sellLocations = ["city center","fortress"]

#generating the world map
def printWorldMap(map):
	for row in map:
		print(" ".join(row))

'''#world map as a dictionary
betterWorldMap = {0:{0:"home",1:"wild",2:"wild",3:"city walls",4:"city center"},
			1:{0:"path",1:"wild",2:"fortress outskirts",3:"city walls",4:"city walls"},
			2:{0:"path",1:"fortress outskirts",2:"fortress",3:"fortress outskirts",4:"wild"},
			3:{0:"wild",1:"wild",2:"fortress outskirts",3:"battlegrounds",4:"battlegrounds"},
			4:{0:"wild",1:"wild",2:"battlegrounds",3:"battlegrounds",4:"battlegrounds"}}'''

#generating the visual world map
worldMapTile = []
for i in range(0,5):
	worldMapTile.append(["O"]*5)
	
playerPositionX = [0]
playerPositionY = [0]

worldMapTile[playerPositionY[0]][playerPositionX[0]] = "X"

#generating a random world map
worldMapLocations = ["home","path","wild","city walls","city center","fortress outskirts","fortress","battlegrounds"]
worldMapHolder = [{} for i in range(5)]
worldMap = {}
for thing in worldMapHolder:
	for i in range(0,5):
		tempList = [random.randint(0,7) for i in range(0,5)]
		for j in range(0,5):
			worldMapHolder[i].update({j:worldMapLocations[tempList[j]]})
for i in range(0,5):
	worldMap[i] = worldMapHolder[i]

#counting the amount of worldMapLocations
locationCount = {}
for i in worldMapLocations:
	locationCount[i] = 0
def countLocationAmount():
	#setting the player starting position to be home/eliminating other instances of home
	for i in worldMap:
		for j in worldMap:
			if worldMap[i][j] == "home":
				worldMap[i][j] = "wild"
	worldMap[playerPositionY[0]][playerPositionX[0]] = worldMapLocations[0]
	for i in worldMap:
		for j in worldMap:
			if worldMap[i][j] == "home":
				locationCount["home"] += 1
			if worldMap[i][j] == "city center":
				locationCount["city center"] += 1			
			if worldMap[i][j] == "wild":
				locationCount["wild"] += 1
			if worldMap[i][j] == "city walls":
				locationCount["city walls"] += 1
			if worldMap[i][j] == "city center":
				locationCount["city center"] += 1
			if worldMap[i][j] == "fortress outskirts":
				locationCount["fortress outskirts"] += 1
			if worldMap[i][j] == "fortress":
				locationCount["fortress"] += 1
			if worldMap[i][j] == "path":
				locationCount["path"] += 1
			if worldMap[i][j] == "battlegrounds":
				locationCount["battlegrounds"] += 1

countLocationAmount()
print(locationCount)

#print(worldMap)

#input custom commands
def incrementAtt(table):
	playerInputQ = input("Please input a command: ")
	target = monsters[0]
	targetSelect = 0
	while True:
		if playerInputQ == "help":
			print("fight: gain stats and things to sell.")
			print("sell: sell your things.")
			print("stats: display information about the array.")
			print("look monsters: monsters attackable in the current tile")
			print("select: select another monster to fight.")
			print("target: current monster targetted.")
			print("monster info: look up information about the current target.")
			print("map: shows your location on the map.")
			print("map info: look up information about the current tile.")
			print("travel: move around the map.")
			print("exit: exit the program.")
		elif playerInputQ == "fight":
			counter = [0]
			if worldMap[playerPositionY[0]][playerPositionX[0]] in monsterLocations[monsters[targetSelect]]:
				while counter[0] < 5 and table[0][0] > 0:
					for i in range(0,5):
						table[0][i] += random.randint(1,5) * monstersStat[targetSelect]
						table[0][0] -= random.randint(0,1) * monstersStat[targetSelect]
						if table[0][0] > 0:
							i += 1
						else:
							print("You died and earned this many numbers.")
							print(table)
							return False
					print("You killed the " + repr(target) + ".")
					print(table)
					counter[0] += 1
					time.sleep(1)
			else:
				print("No monster of that type here.")
		elif playerInputQ == "sell":
			counter = [0]
			if worldMap[playerPositionY[0]][playerPositionX[0]] in sellLocations:
				while counter[0] < 5:
					for i in range(2,5):
						lowerThing = random.randint(0,5)
						if table[0][i] - lowerThing >= 0:
							table[0][i] -= lowerThing
							table[0][1] += lowerThing
						elif table[0][i] - lowerThing < 0 and table[0][i] > 0:
							table[0][1] += table[0][i]
							table[0][i] = 0
						else:
							table[0][i] -= 0
					print(table)
					counter[0] += 1
					time.sleep(1)
			else:
				print("There is no shop here!")
		elif playerInputQ == "exit":
			print("You earned this many numbers!")
			print(table)
			return False
		elif playerInputQ == "select":
			for row in monstersNo:
				print(row)
			try:
				targetSelect = int(input("Please select a target number: ")) - 1
				if targetSelect >= 0 and targetSelect < 6:
					target = monsters[targetSelect]
					print("Target changed to " + repr(monsters[targetSelect]))
				else:
					print("Fuck you.")
			except ValueError:
				print("Fuck you.")
		elif playerInputQ == "target":
			print(target)
		elif playerInputQ == "stats":
			print(table)
			try:
				playerStatsInfoQ = int(input("Which column would you like to know about: ")) - 1
				if playerStatsInfoQ <= 4 and playerStatsInfoQ >= 0:
					print(playerStatsInfo[playerStatsInfoQ])
				else:
					print("Fuck you.")
			except ValueError:
				print("Fuck you.")
		elif playerInputQ == "map":
			printWorldMap(worldMapTile)
		#this really should be its own function
		elif playerInputQ== "travel":
			mapTravelQ = input("Which direction would you like to travel in? ")
			if mapTravelQ == "north":
				try:
					if playerPositionY[0] - 1 >= 0 and playerPositionY[0] - 1 <=4:
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "O"
						playerPositionY[0] -= 1
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "X"
						printWorldMap(worldMapTile)
					else:
						print("This is the edge of the world!")
				except ValueError:
					print("This is the edge of the world!")
			elif mapTravelQ == "south":
				try:
					if playerPositionY[0] + 1 >= 0 and playerPositionY[0] + 1 <=4:
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "O"
						playerPositionY[0] += 1
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "X"
						printWorldMap(worldMapTile)
					else:
						print("This is the edge of the world!")
				except ValueError:
					print("This is the edge of the world!")
			elif mapTravelQ == "east":
				try:
					if playerPositionX[0] + 1 >= 0 and playerPositionX[0] + 1 <=4:
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "O"
						playerPositionX[0] += 1
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "X"
						printWorldMap(worldMapTile)
					else:
						print("This is the edge of the world!")
				except ValueError:
					print("This is the edge of the world!")
			elif mapTravelQ == "west":
				try:
					if playerPositionX[0] - 1 >= 0 and playerPositionX[0] - 1 <=4:
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "O"
						playerPositionX[0] -= 1
						worldMapTile[playerPositionY[0]][playerPositionX[0]] = "X"
						printWorldMap(worldMapTile)
					else:
						print("This is the edge of the world!")
				except ValueError:
					print("This is the edge of the world!")
			else:
				print("Please input a cardinal direction next time.")
		elif playerInputQ == "map info":
			print(worldMap[playerPositionY[0]][playerPositionX[0]])
		elif playerInputQ == "monster info":
			print("+++" + repr(monsters[targetSelect]) + "+++")
			print(monsterExamine[monsters[targetSelect]])
		elif playerInputQ == "look monsters":
			for monster,location in list(monsterLocations.items()):
				if worldMap[playerPositionY[0]][playerPositionX[0]] in location:
					print(monster)
		else:
			print("Invalid input.")
			print("Type 'help' if you need help")
		print("")
		playerInputQ = input("Please input a command: ")

print(playerAttributes)
print("")
incrementAtt(playerAttributes)
